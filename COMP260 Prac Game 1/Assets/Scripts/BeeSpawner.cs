﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {
    public BeeMove beePrefab;
    public int nBees = 0;
    public Rect spawnRect;
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;
    public BeeMove GetBee;


    public float beePeriod;
    public float maxBeePeriod = 3;
    public float minBeePeriod = 0;


    private Transform target;
    private Vector2 heading;
    private float speed;
    private float turnSpeed;

    void OnDrawGizmos()
    {
        // draw the spawning rectangle
        Gizmos.color = Color.green;
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMin, spawnRect.yMin),
                new Vector2(spawnRect.xMax, spawnRect.yMin));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMax, spawnRect.yMin),
                new Vector2(spawnRect.xMax, spawnRect.yMax));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMax, spawnRect.yMax),
                new Vector2(spawnRect.xMin, spawnRect.yMax));
        Gizmos.DrawLine(
                new Vector2(spawnRect.xMin, spawnRect.yMax),
                new Vector2(spawnRect.xMin, spawnRect.yMin));
    }




   



	// Use this for initialization
	void Start () {
        StartCoroutine(SpawnBees());
       


	}

    IEnumerator SpawnBees(){
        yield return new WaitForSeconds(Random.Range(minBeePeriod, maxBeePeriod));
        BeeMove bee = Instantiate(beePrefab);
        bee.transform.parent = transform;
        bee.gameObject.name = ("Bee" + 1);
        float x = spawnRect.xMin + Random.value * spawnRect.width;
        float y = spawnRect.yMin + Random.value * spawnRect.height;

        bee.transform.position = new Vector2(x, y);
        StartCoroutine(SpawnBees());
    }
	
	// Update is called once per frame
    void Update () {
       
	}


    public void DestroyBees(Vector2 centre, float radius)
    {
        // destroy all bees within ‘radius’ of ‘centre’
        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);
            // Fixed bug by adding a type conversion
            Vector2 v = (Vector2)child.position - centre;
            if (v.magnitude <= radius)
            {
                Destroy(child.gameObject);
            }
        }
    }

}
