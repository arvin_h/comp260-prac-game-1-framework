﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {
    public float speed = 4.0f;        // metres per second
    public float turnSpeed = 180.0f;  // degrees per second
    public Transform target1;
    public Transform target2;
    private Vector2 heading = Vector2.right;
    public Vector2 close = Vector2.zero;
    public float minSpeed, maxSpeed;
    public float minTurnSpeed, maxTurnSpeed;
    public ParticleSystem explosionPrefab;


    // private state

    private Transform target;
   





    void Start()
    {
        // instantiate a bee
        PlayerMove player =
            (PlayerMove)FindObjectOfType(typeof(PlayerMove));
        target1 = player.transform;

        //heading = Vector2.right;
        //float angle = Random.value * 360;
        //heading = heading.Rotate(angle);

        //// set speed and turnSpeed randomly 
        //speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
        //turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed,
                                 //Random.value);



    }


	void Update()
    {
        var distance = heading.magnitude;
        // get the vector from the bee to the target 
        Vector2 direction = target1.position - transform.position;
        Vector2 direction2 = target2.position - transform.position;

        if ( direction.magnitude > direction2.magnitude){
            close = direction2;
        }
        else {
            close = direction;
        }

        // calculate how much to turn per frame
        float angle = turnSpeed * Time.deltaTime;

        // turn left or right
        if (close.IsOnLeft(heading))
        {
            // target on left, rotate anticlockwise
            heading = heading.Rotate(angle);
        }
        else
        {
            // target on right, rotate clockwise
            heading = heading.Rotate(-angle);
        }

        transform.Translate(heading * speed * Time.deltaTime);
    }
//    void OnDrawGizmos()
//    {
//        // draw heading vector in red
//        Gizmos.color = Color.red;
//        Gizmos.DrawRay(transform.position, heading);

//        // draw target vector in yellow
//        Gizmos.color = Color.yellow;
//        Vector2 direction = target1.position - transform.position;
//        Gizmos.DrawRay(transform.position, direction);
//    }
    void OnDestroy()
    {
        // create an explosion at the bee's current position
        ParticleSystem explosion = Instantiate(explosionPrefab);
        explosion.transform.position = transform.position;
        Destroy(explosion.gameObject, explosion.duration);

    }


}
