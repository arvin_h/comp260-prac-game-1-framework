﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public Vector2 move;
    public Vector2 velocity;
    public float MaxSpeed = 5.0f;
    public float acceleration = 1.0f;
    private float speed = 0.0f;
    public float brake = 5.0f;
    public float turnSpeed = 30.0f;
    public float Player;
    public float destroyRadius = 1.0f; 



    private BeeSpawner beeSpawner;


    // Use this for initialization
    void Start()
    {
        beeSpawner = FindObjectOfType<BeeSpawner>();
        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
                transform.position, destroyRadius);
        }


    }

    // Update is called once per frame
    void Update()

    {
        if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
                transform.position, destroyRadius);
        }

        float turn;

        float forwards;

        if (this.gameObject.tag == "Player")
        {

            turn = Input.GetAxis("H1");
            forwards = Input.GetAxis("V1");
        }
        else
        {
            turn = Input.GetAxis("H2");
            forwards = Input.GetAxis("V2");
        }

       


        if (forwards > 0)
        {
            transform.Rotate(0, 0, -turn * turnSpeed * speed * Time.deltaTime);


            speed = speed + acceleration * Time.deltaTime;

           
        }
        else if (forwards < 0)
        {
            transform.Rotate(0, 0, -turn * turnSpeed * speed * Time.deltaTime);

            speed = speed - acceleration * Time.deltaTime;
        }
        else
        {
            if (speed > 0)
            {
                speed = Mathf.Max(speed - brake * Time.deltaTime , 0);
            }
            else 
            {
                speed = Mathf.Min(speed + brake * Time.deltaTime , 0);
            }
        }

        speed = Mathf.Clamp(speed, -MaxSpeed, MaxSpeed);

        Vector2 velocity = Vector2.up * speed;

        if(forwards != 0){
            transform.Translate(velocity * Time.deltaTime, Space.Self);   
        }
       




        //    Vector2 direction;

        //    if (this.gameObject.tag == "Player") { 

        //    direction.x = Input.GetAxis("H1");
        //    direction.y = Input.GetAxis("V1");
        //}
        //    else {
        //        direction.x = Input.GetAxis("H2");
        //        direction.y = Input.GetAxis("V2");
        //    }
        //    Vector2 velocity2 = direction * MaxSpeed;



        //    transform.Translate(velocity2 * Time.deltaTime);
        //}
    }

  
}
